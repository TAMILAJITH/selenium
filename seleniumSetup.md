# <div align="center">Selenium Java Setup in VS code</div>

<br>

# <div align="center">Overview</div>

>> ## 1.Install Java Extensions.
>> ## 2.JDK Installation –jdk.11.
>> ## 3.Maven Installation. 

<br>


## 1.Install Java Extensions :


  >- ### Click extensions icon that show in left side corner of VS code.

![JavaExtension](Images/image1.jpg)
<br><br>
   
 >- ### Type java extension in search box.

![image2](Images/image2.jpg)
<br><br>
>- ### Install java extension pack.
 
![image3](Images/Image3.jpg)
<br><br>

# 2.JDK Installation –jdk.11 :

>- ### Click F1 in VS code.
>>>> Popup come from top of VS code. 

![image4](Images/Image4.jpg)
<br><br>

>- ### Select Configure Java Runtime option or type Configure Java Runtime in search box. 

![image5](Images/Image5.jpg)
<br><br>
>- ### Select JDK version-11 and click download button.


![Image6](Images/Image6.jpg)

<br><br>

# 3.Maven Installation :

>- ### Go to below given URL.

>>> --> https://maven.apache.org/download.cgi

![Image7](Images/Image7.jpg)

<br>

>- ### Download  apache-maven-3.6.3-bin.zip  file.

![Image8](Images/Image8.jpg)

<br>

>- ### Extract zip file

![Image9](Images/Image9.jpg)

<br>

>- ### Move  file  to Local Disk C.
>>>> For safety purpose.

<br>

>- ### Open bin folder in apache-maven-3.6.3-bin  file.

![Image10](Images/Image10.jpg)

<br>

>- ### Copy path of bin folder

![Image11](Images/Image11.jpg)

<br>

>- ### Open setting in VS code.

>>> By shortcut ctrl+, or click setting icon that show in left side corner of VS code.

<br>

>- ### Type maven path in search box.

![Image12](Images/Image12.jpg)

<br>

>- ### Paste your path with /mvn.cmd in Maven Executable path.
>>>  Like C:\apache-maven-3.6.3-bin\apache-maven-3.6.3\bin/mvn.cmd.

![Image13](Images/Image13.jpg)

<br>

>- ### Restart your VS code.

<br>

>- ### Now two extension that java project and maven show in left side bottom of VS code.

![Image14](Images/Image14.jpg)

<br>

# <div align="center">Finished</div>